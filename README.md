- $make galaxy #pulls in requirements.yml
- fix ansible_hosts accordingly
- $ansible-playbook -e 'ansible_python_interpreter=/usr/bin/python3' --user deploy --private-key=~/.ssh/dev-gldo -i ./ansible_hosts --limit mk main.yml
-

This example uses multiple stages with tests/linting and push style deployments (vs pull)
