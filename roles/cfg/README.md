Role Name
=========

Pulls the system environment configuration repo and sets symlinks and other 
useful settings

Requirements
------------

None

Role Variables
--------------

TBD

Dependencies
------------

None

Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

    - hosts: servers
      roles:
         - { role: TBD  }

License
-------

GPL 2

Author Information
------------------

Nathan Black
